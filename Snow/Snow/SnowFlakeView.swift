//
//  SnowFlakeView.swift
//  Snow
//
//  Created by reinier van vliet on 19/11/2019.
//  Copyright © 2019 reinier van vliet. All rights reserved.
//

import UIKit

class SnowFlakeView: UIImageView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        image = UIImage.circle(diameter: frame.size.width, color: .white)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
