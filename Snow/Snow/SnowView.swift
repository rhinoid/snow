//
//  SnowView.swift
//  Snow
//
//  Created by reinier van vliet on 12/11/2019.
//  Copyright © 2019 reinier van vliet. All rights reserved.
//

import UIKit

class Flake {
     init(x: Float,
    y: Float,
    z: Float,
    xSpd: Float,
    ySpd: Float,
    xFlutter: Float,
    yFlutter: Float,
    flakeView: UIView
     ) {
     self.x = x
     self.y = y
     self.z = z
     self.xSpd = xSpd
     self.ySpd = ySpd
     self.xFlutter = xFlutter
     self.yFlutter = yFlutter
     self.flakeView = flakeView
     }
    var x: Float
    var y: Float
    var z: Float
    var xSpd: Float
    var ySpd: Float
    var xFlutter: Float
    var yFlutter: Float
    var flakeView: UIView
}

class SnowView: UIView {
    var flakes = [Flake]()
    private var displayLink: CADisplayLink?
    private var startTime = Date()
    private let flakeSize = Float(34.0)
    private let minZ: Float = 1.0
    private let maxZ: Float = 3.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initFlakes(numFlakes: Int) {
        setGradient((UIColor(hexString: "#03295F"), UIColor(hexString: "#000F1F")), angle: Float.pi / 2)
        
        for _ in 0 ..< numFlakes {
            let z = Float.random(in: minZ ..< maxZ)
            let flakeView = SnowFlakeView(
                frame: CGRect(
                    x: 0.0,
                    y: 0.0,
                    width: CGFloat(flakeSize/z),
                    height: CGFloat(flakeSize/z)
                )
            )
            flakeView.alpha = CGFloat((z-minZ) / (maxZ-minZ))
            let flake = Flake(
                x: Float.random(in: -Float(frame.width) ..< Float(frame.width)),
                y: Float.random(in: -Float(frame.height) ..< Float(frame.height)),
                z: z,
                xSpd: Float.random(in: -0.8 ..< -0.7),
                ySpd: Float.random(in: 1.6 ..< 1.7),
                xFlutter: Float.random(in: 0.0 ..< Float.pi),
                yFlutter: Float.random(in: 0.0 ..< Float.pi),
                flakeView: flakeView)
            flakes.append(flake)
            addSubview(flakeView)
        }
        
        displayLink = CADisplayLink(target: self, selector: #selector(update))
        displayLink?.add(to: .current, forMode: .common)
        startTime = Date()
    }
    var posZ: Float = -0.9

    @objc func update() {
        let t = Float(Date().distance(to: startTime))
        for flake in flakes {
            flake.x += flake.xSpd
            flake.y += flake.ySpd
            if flake.x < -Float(frame.width) {
                flake.x += Float(frame.size.width * 2.0)
            }

            if flake.y > Float(frame.height) {
                flake.y -= Float(frame.size.height * 2.0)
            }

            var x = ((flake.x + sinf(flake.xFlutter + t) * 20.0)) / (flake.z + posZ)
            var y = ((flake.y + sinf(flake.yFlutter + t) * 8.0)) / (flake.z + posZ)
            x += Float(center.x)
            y += Float(center.y)
            flake.flakeView.center = CGPoint(x: CGFloat(x), y: CGFloat(y))
        }
    }
}
