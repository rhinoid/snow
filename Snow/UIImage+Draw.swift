//
//  UIImage+Draw.swift
//  Snow
//
//  Created by reinier van vliet on 20/11/2019.
//  Copyright © 2019 reinier van vliet. All rights reserved.
//

import UIKit

extension UIImage {
    class func circle(diameter: CGFloat, color: UIColor) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: diameter, height: diameter), false, 0)
        guard let ctx = UIGraphicsGetCurrentContext() else { return nil }

        ctx.saveGState()

        ctx.addEllipse(in: CGRect(x: 0.0, y: 0.0, width: diameter, height: diameter))
        ctx.clip()

        
        let colorSpace = CGColorSpaceCreateDeviceRGB()

        let colors:[CGColor] = [UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0).cgColor , UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.0).cgColor]

        let gradient = CGGradient(colorsSpace: colorSpace, colors: colors as CFArray, locations: [0.0,1.0])!

        let center = CGPoint(x: diameter/2, y: diameter/2)
        _ = CGPoint(x: 0, y: diameter)
        _ = CGPoint(x: diameter, y: diameter)

        ctx.drawRadialGradient(gradient, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: diameter/2, options: [])
        
        ctx.restoreGState()
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return img
    }
    
    func addBlur() -> UIImage? {
        if let ciImg = CIImage(image: self) {
            ciImg.applyingFilter("CIGaussianBlur", parameters: [kCIInputRadiusKey: 5.0])
            return UIImage(ciImage: ciImg)
        }
        return nil
    }
}
