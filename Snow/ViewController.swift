//
//  ViewController.swift
//  Snow
//
//  Created by reinier van vliet on 12/11/2019.
//  Copyright © 2019 reinier van vliet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        let snow = SnowView(frame: view.frame)
        snow.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        view.addSubview(snow)
        snow.initFlakes(numFlakes: 200)
    }


}

