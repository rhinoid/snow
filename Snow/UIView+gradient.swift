//
//  UIView+gradient.swift
//  Snow
//
//  Created by reinier van vliet on 19/11/2019.
//  Copyright © 2019 reinier van vliet. All rights reserved.
//

import UIKit

public extension UIView {
    // Angle in radians. Default is right to left
    func setGradient(_ gradient: (UIColor, UIColor), angle: Float = Float.pi) {
        setGradient([gradient.0.cgColor, gradient.1.cgColor], angle: angle)
    }

    func setGradient(_ fromColor: UIColor, _ toColor: UIColor, angle: Float = Float.pi) {
        setGradient([fromColor.cgColor, toColor.cgColor], angle: angle)
    }

    func setGradient(colors: [CGColor], locations: [NSNumber], angle: Float) {
        setGradient(colors, locations: locations, angle: angle)
    }

    private func setGradient(_ gradient: [CGColor], locations: [NSNumber] = [0.0, 1.0], angle: Float) {
        let pointOnRectangle = pointOnGradientRectangleGivenAngle(angle: angle)
        let oppositePointOnRectangle = CGPoint(x: 1.0 - pointOnRectangle.x, y: 1.0 - pointOnRectangle.y)

        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: Int(bounds.size.width), height: Int(bounds.size.height))
        gradientLayer.colors = gradient
        gradientLayer.startPoint = pointOnRectangle
        gradientLayer.locations = locations
        gradientLayer.endPoint = oppositePointOnRectangle
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)

        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            let img = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            layer.contents = img!.cgImage
        }
    }

    // In Cocoa gradients are sloped via 2 points on a rectangle of size 1 by 1 located at the origin.
    // So if the start point is e.g. (0.0, 0.0) and the endpoint (1.0, 1.0)   then the grdient will go from the bottom corner to the topright
    // And if for instance the first point is (0.0, 0.5) and the second (1.0, 0.5) then the gradient goes from left to right.
    // This method calculates these start and endpoints, given the angle of the gradient's slope
    private func pointOnGradientRectangleGivenAngle(angle: Float) -> CGPoint {
        // Calc x,y Point on Unit circle
        let xOnCircle = cos(angle)
        let yOnCircle = sin(angle)

        // Determine if the X or Y part is the longest and use that
        let maxLength = max(abs(xOnCircle), abs(yOnCircle))

        // Calc the intersection with the Unit Square (by normalizing)
        let xOnRectangle = xOnCircle / maxLength
        let yOnRectangle = yOnCircle / maxLength

        // Transform Unit Square to Square from (0.0, 0.0) - (1.0, 1.0)
        let xOnTransformedRectangle = xOnRectangle / 2.0 + 0.5
        let yOnTransformedRectangle = yOnRectangle / 2.0 + 0.5

        return CGPoint(x: CGFloat(xOnTransformedRectangle), y: CGFloat(yOnTransformedRectangle))
    }
}
